## Setup Instructions  
1. Make sure you have the following items installed:  
  a. git client (download [here](https://git-scm.com/downloads))  
  b. composer (download [here](https://getcomposer.org/download/))  
2. Open Git Bash terminal
3. Go to htdocs: `cd /c/xampp/htdocs`
4. Clone this repository: `git clone https://gitlab.com/csci2412/examples/form-handling.git`
5. Go into repository directory: `cd form-handling`
6. Install dependencies: `composer install` (this example does not have any dependencies but it is good to get used to doing this)
7. You should now be able to view and edit the source code from Visual Studio Code or whatever text editor you are using