<?php
$title = 'Week 4 - Handling Form Submissions';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=$title?></title>
	<!-- https://necolas.github.io/normalize.css/ -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css" rel="stylesheet">
	<link href="assets/main.css" rel="stylesheet">
</head>
<body>
<main>
	<h1><?=$title?></h1>
	<form>
		<fieldset>
			<legend>Contact Us</legend>
			<div class="input-group">
				<textarea placeholder="Write your comment here"></textarea>
			</div>
			<button type="submit">Submit</button>
		</fieldset>
	</form>
</main>
</body>
</html>